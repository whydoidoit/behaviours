const HookedEvents = require('alcumus-local-events/hooked-events')
const isObject = require('lodash/isObject')
const isString = require('lodash/isString')
const forEach = require('lodash/forEach')
const merge = require('lodash/merge')
const sortBy = require('lodash/sortBy')
const isArray = require('lodash/isArray')

const DEFAULT = 'default'

class Cancel extends Error {
    constructor(...args) {
        super(...args)
        Error.captureStackTrace(this, Cancel)
    }
}

function ensureArray(param) {
    return Array.isArray(param) ? param : [param]
}

let availableBehaviours = {}

function doNotMapState(state) {
    return state
}

const DUMMY = {}

let events = new HookedEvents({wildcard: true, maxListeners: 1000, delimiter: '.'})

function reset() {
    events.removeAllListeners()
    availableBehaviours = {}
}

function isEqual(test, item) {
    let isSame = true
    forEach(item, (value, key) => {
        isSame = isSame && test[key] == value
    })
    return isSame
}

function stringify(source, replacer, space) {
    const data = {source, replacer, space}
    events.emit('behaviour.stringify', data)
    data.result = JSON.stringify(data.source, data.replacer, data.space)
    events.emit('behaviour.stringified', data)
    return data.result
}

function parse(source, reviver) {
    const data = {source, reviver}
    const fixUps = []
    events.emit('behaviour.parse', data)
    data.result = JSON.parse(data.source, (key, value) => {
        if (isObject(value) && value._behaviours) {
            fixUps.push(() => initialize(value))
        }
        if (data.reviver) {
            data.reviver(key, value)
        }
        return value
    })
    events.emit('behaviour.parsed.pre', data)
    fixUps.forEach(fixupFunction => fixupFunction())
    events.emit('behaviour.parsed.post', data)
    return data.result
}

function resolveMethods(definition) {
    let methods = definition.methods || {}
    let allMethods = definition.allMethods = new Set()
    events.emit('default-methods', methods, definition)
    Object.keys(methods).forEach(function (key) {
        if (typeof methods[key] !== 'function') {
            throw new Error('Only functions may be methods')
        }
        allMethods.add(key)
    })
    return allMethods
}

function register(name, definition, allowMultiple) {
    let existing = undefined
    if (!isObject(definition)) {
        throw new Error('The definition must be an object')
    }
    if (!isString(name)) {
        throw new Error('The name must be a string')
    }
    if ((availableBehaviours[name]) && !allowMultiple) {
        throw new Error(`Behaviour '${name}' already registered`)
    }
    let defaults = definition.defaults
    if (defaults) {
        Object.keys(defaults).forEach(function (key) {
            if (typeof defaults[key] === 'function') {
                throw new Error('Defaults must not include functions')
            }
        })
    }
    let allMethods = resolveMethods(definition)
    let states = definition.states = definition.states || DUMMY
    Object.keys(states).forEach(function (stateName) {
        let state = states[stateName]
        Object.keys(state.methods || DUMMY).forEach(function (key) {
            if (typeof state.methods[key] !== 'function') {
                throw new Error('Only functions may be methods')
            }
            allMethods.add(key)
        })
    })
    let list = availableBehaviours[name] = availableBehaviours[name] || []
    list.push(definition)
}

function tryToCall(behaviourName, instance, method, ...params) {
    if (events.emit.apply(events, [`${behaviourName}.${method}`, instance, ...params])) {
        let toCallBehaviours = ensureArray(availableBehaviours[behaviourName])
        let called = false
        for (let behaviour of toCallBehaviours) {
            if (behaviour) {
                let fn = behaviour[method]
                if (fn && (!fn.once || !called)) {
                    called = true
                    return fn.apply(instance, params)
                }
            }
        }
    }
}

function initialize(target, addBehaviours = {}) {
    if (target.behaviours) return
    const api = {}

    let temporaryInstances = []
    let reasons = []
    let behaviours = target._behaviours = Object.defineProperties(Object.assign({
            instances: {},
            _state: '',
            sendMessage,
            sendMessageAsync,
            destroy,
            add,
            remove,
            setState
        },
        target._behaviours),
        {
            behaviours: {
                get() {
                    return target._behaviours
                },
            },
            reasons: {
                get() {
                    return reasons
                }
            },
            state: {
                get() {
                    return behaviours._state || DEFAULT
                },
                set(endState) {
                    endState = endState || DEFAULT
                    let startState = behaviours.state
                    if (startState === endState) return
                    let data = {canChange: true, startState, endState, reasons: []}
                    forEachBehaviour((instance, behaviour) => {
                        let mappingFunction = (behaviour.mapState || doNotMapState).bind(instance)
                        let behaviourStart = mappingFunction(startState)
                        let behaviourEnd = mappingFunction(endState)
                        let oldState = (behaviour.states || DUMMY)[behaviourStart]
                        let newState = (behaviour.states || DUMMY)[behaviourEnd]
                        if (oldState && oldState.canExit) {
                            oldState.canExit.call(instance, data)
                        }
                        if (newState && data.canChange && newState.canEnter) {
                            newState.canEnter.call(instance, data)
                        }
                    })
                    reasons = data.reasons
                    if (!data.canChange) return
                    reasons = []
                    delete data.canChange
                    forEachBehaviour((instance, behaviour) => {
                        let mappingFunction = (behaviour.mapState || doNotMapState).bind(instance)
                        let behaviourStart = mappingFunction(startState)
                        let oldState = (behaviour.states || DUMMY)[behaviourStart]
                        if (oldState && oldState.exit) {
                            let result = oldState.exit.call(instance, data)
                            instance._ready = result && result.then ? result : null
                        } else {
                            instance._ready = null
                        }
                    })
                    behaviours._state = endState
                    behaviours.sendMessage('stateChanged', data)
                    events.emit('stateChanged', target, endState, data)
                    forEachBehaviour((instance, behaviour) => {
                        let mappingFunction = (behaviour.mapState || doNotMapState).bind(instance)
                        let behaviourEnd = mappingFunction(endState)
                        let newState = (behaviour.states || DUMMY)[behaviourEnd]
                        if (newState && newState.enter) {
                            if (instance._ready) {
                                instance._ready = instance._ready.then(() => Promise.resolve(newState.enter.call(instance, data)))
                            } else {
                                instance._ready = Promise.resolve(newState.enter.call(instance, data))
                            }
                        }
                    })
                    behaviours.ready = Promise.all(getAllInstances().map(pair => pair.instance._ready).concat([behaviours.sendMessageAsync('stateChangeComplete', data)]))
                }
            }
        })

    forEach(Object.assign({}, behaviours.instances, addBehaviours), function (list, type) {
        list = isArray(list) ? list : [list]
        list.forEach(function (instance) {
            behaviours.add(type, instance)
        })
    })


    Object.defineProperties(target, {
        behaviours: {
            get() {
                return behaviours
            }
        },
        methods: {
            get() {
                return api
            }
        },
        api: {
            get() {
                return api
            }
        },
        setState: {
            get() {
                return behaviours.setState
            }
        },
        sendMessage: {
            get() {
                return sendMessage
            }
        },
        sendMessageAsync: {
            get() {
                return sendMessageAsync
            }
        }
    })

    api.setState = setState
    api.document = api.$ = target

    return target

    function setState(endState) {
        behaviours.state = endState
        return behaviours.ready
    }

    async function remove(behaviourName, instance = null) {
        if (!instance) {
            let current = behaviours.instances[behaviourName] || []
            current.forEach(instance => {
                tryToCall(behaviourName, instance, 'destroy')
            })
            delete behaviours.instances[behaviourName]
        } else {
            let list = behaviours.instances[behaviourName] = behaviours.instances[behaviourName] || []
            let instanceIndex = list.indexOf(instance)
            let temporaryIndex = temporaryInstances.findIndex(item => item.instance === instance)
            if (instanceIndex !== -1 || temporaryIndex !== -1) {
                tryToCall(behaviourName, instance, 'destroy')
            }
            list.splice(instanceIndex, 1)
            temporaryInstances.splice(temporaryIndex, 1)
            if (!list.length) delete behaviours.instances[behaviourName]
        }
    }

    function destroy() {
        let instances = behaviours.instances
        for (let type in instances) {
            behaviours.remove(type)
        }
        temporaryInstances.forEach(pair => {
            tryToCall(pair.type, pair.instance, 'destroy')
        })
        temporaryInstances.length = 0
    }

    function getAllInstances() {
        let instances = temporaryInstances.slice(0)
        forEach(behaviours.instances, function (list, type) {
            list.forEach(instance => {
                instances.push({type, instance})
            })
        })
        return instances
    }

    function forEachBehaviour(cb) {
        forEach(behaviours.instances, function (list, type) {
            let toCallBehaviours = ensureArray(availableBehaviours[type])
            for (let availableBehaviour of toCallBehaviours) {
                if (availableBehaviour) {
                    list.forEach(instance => {
                        cb(instance, availableBehaviour)
                    })
                }
            }
        })
    }

    function add(behaviourName, instance, temporary = false) {
        instance = instance || {}
        let existing = availableBehaviours[behaviourName]
        if (!existing) {
            const data = {availableBehaviour: null, behaviourName, instance}
            //Allow an event to modify the behaviour and instance
            events.emit(`behaviour.add.${behaviourName}`, data)
            existing = data.availableBehaviour
            availableBehaviours[behaviourName] = existing
            instance = data.instance
        }
        if (!existing && instance._mandatory !== false) {
            throw new Error(`Behaviour '${behaviourName}' does not exist`)
        }

        Object.defineProperties(instance, {
            document: {
                get() {
                    return target
                }
            },
            methods: {
                get() {
                    return api
                }
            },
            api: {
                get() {
                    return api
                }
            },
            $: {
                get() {
                    return target
                }
            },
            _: {
                get() {
                    return api
                }
            },
            sendMessage: {
                get() {
                    return sendMessage
                }
            },
            sendMessageAsync: {
                get() {
                    return sendMessageAsync
                }
            }
        })

        Object.defineProperty(instance, 'destroy', {
            get() {
                return () => behaviours.remove(behaviourName, instance)
            }
        })

        let addBehaviours = Array.isArray(existing) ? existing : [existing]
        for (let availableBehaviour of addBehaviours) {
            merge(instance, availableBehaviour.defaults || {}, instance)
            if (availableBehaviour.calls) {
                for (let call of availableBehaviour.calls) {
                    api[call] = api[call] || ((...params) => sendMessage(call, ...params))
                }
            }
            if (availableBehaviour.methods) {
                for (let fn in availableBehaviour.methods) {
                    ((fn) => {
                        let fns = instance[`__${fn}`]
                        if (instance[fn] && typeof instance[fn] !== 'function') {
                            throw new Error(`Member "${fn}" already declared`)
                        }
                        if (!fns) {
                            fns = instance[`__${fn}`] = []

                            function getter(...params) {
                                let result = undefined
                                for (let fn of fns) {
                                    let newResult = fn(...params)
                                    if (result && result.then) {
                                        result = result.then(Promise.resolve(newResult))
                                    } else {
                                        result = newResult
                                    }
                                }
                                return result
                            }

                            getter.once = true
                            Object.defineProperties(instance, {
                                [`__${fn}`]: {
                                    get() {
                                        return fns
                                    },
                                    configurable: false,
                                    enumerable: false
                                },
                                [fn]: {
                                    get() {
                                        return getter
                                    },
                                    set() {
                                        throw new Error(`Method "${fn}" cannot be overwritten`)
                                    },
                                    configurable: false,
                                    enumerable: false
                                }
                            })
                        }
                        fns.push(availableBehaviour.methods[fn].bind(instance))
                    })(fn)
                }
            }

            if (!availableBehaviour.allMethods) resolveMethods(availableBehaviour)
            availableBehaviour.allMethods.forEach(method => {
                if (!api[method]) {

                    api[method] = function (...params) {
                        return sendMessage(method, ...params)
                    }
                }
            })

            if (availableBehaviour.mandatory === false) {
                instance._mandatory = false
            }

            let newState = (availableBehaviour.states || DUMMY)[behaviours.state] || DUMMY
            if (newState.enter) {
                let data = {startState: null, endState: behaviours.state}
                newState.enter.call(instance, data)
            }

            if (availableBehaviour.requires) {
                if (Array.isArray(availableBehaviour.requires)) {
                    forEach(availableBehaviour.requires, function (type) {
                        if (!behaviours.instances[type]) {
                            behaviours.add(type, {})
                        }
                    })
                } else {
                    forEach(availableBehaviour.requires, function (list, type) {
                        list = isArray(list) ? list : [list]
                        list.forEach(function (instance) {
                            if (!instance.document && (!behaviours.instances[type] || -1 === behaviours.instances[type].findIndex(item => isEqual(item, instance)))) {
                                behaviours.add(type, instance)
                            }
                        })
                    })
                }
            }
        }

        function notify() {
            tryToCall(behaviourName, instance, 'initialize')
            setTimeout(function () {
                tryToCall(behaviourName, instance, 'postInitialize')
            }, 0)
        }

        if (!temporary && availableBehaviours.temporary !== true) {
            let list = behaviours.instances[behaviourName] = behaviours.instances[behaviourName] || []
            if (list.indexOf(instance) === -1) {
                notify()
                list.push(instance)
            }
        } else {
            temporaryInstances.push({type: behaviourName, instance})
            notify()
        }

        return instance
    }

    function noop(v) {
        return v
    }

    function sendMessage(message, ...params) {
        let context = {}
        let result = undefined
        let promises = []
        let count = 0

        events.emit('willSendMessage', {target, message, params, context})
        let toCall = sortBy(getAllInstances(), pair => pair.instance._priority || 100)

        try {
            toCall.filter(t => availableBehaviours[t.type]).forEach(({type, instance}) => {
                (instance[`before_${message}`] || noop).apply(instance, [...params, result])
            })
            toCall.filter(t => availableBehaviours[t.type]).forEach(({type, instance}) => {
                let toCallBehaviours = ensureArray(availableBehaviours[type])
                let called = false
                for (let availableBehaviour of toCallBehaviours) {
                    let states = availableBehaviour.states || DUMMY
                    let mappingFunction = (availableBehaviour.mapState || doNotMapState).bind(instance)
                    let behaviourState = mappingFunction(behaviours.state)
                    let state = states[behaviourState || DEFAULT] || DUMMY
                    let methods = state.methods || DUMMY
                    let method = methods[message]
                    let fn = instance[message]
                    let before = instance[`before_${message}`] || noop
                    let after = instance[`after_${message}`] || noop

                    if (events.emit.apply(events, [`${type}.${message}`, instance, ...params])) {
                        if (method) {
                            count++
                            result = method.apply(instance, [...params, result])
                            if (result && result.then) {
                                promises.push(result)
                            }
                        } else if (fn && (!fn.once || !called)) {
                            called = true
                            if (typeof fn === 'function') {
                                count++
                                result = fn.apply(instance, params, [...params, result])
                                if (result && result.then) {
                                    promises.push(result)
                                }
                            }
                        }
                    }
                }
            })

        } catch (e) {
            if (!(e instanceof Cancel)) throw e
        }
        params.called = count
        params.result = result
        events.emit('hasSentMessage', {target, message, params, context, result})
        if (promises.length) {
            let result = Promise.all(promises).then(r => params.length >= 1 ? (Array.isArray(params[0]) ? Object.assign(params[0], {
                count,
                result
            }) : params) : params)
            after(toCall, message, params)
            return result

        } else {
            after(toCall, message, params)
            return params.length >= 1 ? (Array.isArray(params[0]) ? Object.assign(params[0], {
                count,
                result
            }) : params) : params
        }


    }

    async function sendMessageAsync(message, ...params) {
        let promises = []
        let count = 0
        let context = {}
        events.emit('willSendMessage', {target, message, params, context})
        let toCall = sortBy(getAllInstances(), pair => pair.instance._priority || 100)
        toCall.filter(t => availableBehaviours[t.type]).forEach(({type, instance}) => {
            (instance[`before_${message}`] || noop).apply(instance, params)
        })

        await Promise.all(toCall.filter(t => availableBehaviours[t.type]).map(async ({type, instance}) => {
            let toCallBehaviours = ensureArray(availableBehaviours[type])
            let called = false
            for (let availableBehaviour of toCallBehaviours) {
                let states = availableBehaviour.states || DUMMY
                let mappingFunction = (availableBehaviour.mapState || doNotMapState).bind(instance)
                let behaviourState = mappingFunction(behaviours.state)
                let state = states[behaviourState || DEFAULT] || DUMMY
                let methods = state.methods || DUMMY
                let method = methods[message]
                let fn = instance[message]

                if (await events.emitAsync.apply(events, [`${type}.${message}`, instance, ...params])) {
                    if (method) {
                        count++
                        promises.push(Promise.resolve(method.apply(instance, params)))
                    } else if (fn && (!fn.once || !called)) {
                        called = true
                        if (typeof fn === 'function') {
                            count++
                            promises.push(Promise.resolve(fn.apply(instance, params)))
                        }
                    }
                }
            }
        }))
        params.called = count
        await Promise.all(promises)
        after(toCall, message, params)
        events.emit('hasSentMessage', {target, message, params, context, count})
        return params.length >= 1 ? (Array.isArray(params[0]) ? Object.assign(params[0], {
            count,
            result
        }) : params) : params
    }

}

function after(toCall, message, params) {
    let called = false
    try {
        toCall.filter(t => availableBehaviours[t.type]).forEach(({type, instance}) => {
            const call = instance[`after_${message}`]
            if (call && (!call.once || !called)) {
                call.apply(instance, params)
            }
        })
    } catch (e) {
        if (!(e instanceof Cancel)) throw e
    }
}

module.exports = {
    events,
    availableBehaviours,
    register,
    initialize,
    reset,
    stringify,
    parse,
    Cancel
}
