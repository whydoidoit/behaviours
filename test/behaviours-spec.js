const expect = require('chai').expect;
const Behaviours = require('../index');
const fs = require('fs')
const path = require('path')

describe("Behaviours", function () {
    beforeEach(function () {
        Behaviours.reset();
    });
    it("should parse the file", function() {
        let file = fs.readFileSync(path.join(__dirname, '..', 'audit.json'), 'utf8')
        let item = {test: " test"}
        item.test = String.fromCharCode(31) + item.test
        let str = JSON.stringify(item).replace(/\\u00(0|1)[0-9a-f]/ig, '')
        console.log(str)
        let top = JSON.parse(str);

    })

    it("should initialize an object to use behaviours", function () {
        const test = {};
        Behaviours.initialize(test);
        expect(test).to.have.property('_behaviours');
        expect(test.behaviours).to.exist;
    });
    it("should not add a behaviour that doesn't exist", function () {
        const test = {};
        Behaviours.initialize(test);
        expect(function () {
            test.behaviours.add('test');
        }).to.throw("Behaviour 'test' does not exist");

    });
    it("should allow a behaviour to be registered", function () {
        const test = {};
        expect(function () {
            Behaviours.register();
        }).to.throw("The definition must be an object");
        expect(function () {
            Behaviours.register(1, {});
        }).to.throw("The name must be a string");
        Behaviours.register("test", {});
        Behaviours.initialize(test);
        test.behaviours.add('test');
    });
    it("shouldn't allow the same behaviour to be registered twice", function () {
        Behaviours.register("test", {});
        expect(function () {
            Behaviours.register("test", {});
        }).to.throw("Behaviour 'test' already registered");
    });
    it("should add behaviours to an object", function () {
        const test = {};
        Behaviours.register("test", {});
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 1});
        test.behaviours.add('test', {a: 2});
        expect(test.behaviours.instances.test.length).to.equal(2);
        expect(test.behaviours.instances.test[0].a).to.equal(1);
        expect(test.behaviours.instances.test[1].a).to.equal(2);
    });
    it("should have a document reference on each behaviour", function () {
        const test = {};
        Behaviours.register("test", {});
        Behaviours.register("banana", {});
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 1});
        test.behaviours.add('banana', {a: 2});
        expect(test.behaviours.instances.banana[0].document).to.equal(test);
        expect(test.behaviours.instances.banana[0].$).to.equal(test);
        expect(test.behaviours.instances.banana[0]._).to.equal(test.methods);
        expect(test.behaviours.instances.test[0].document).to.equal(test);
    });
    it("should fire an initialize method on adding each instance", function () {
        let counter = 0;
        const test = {};
        Behaviours.register("test", {
            initialize() {
                counter++;
                expect(this.a).to.be.greaterThan(0);
                expect(this.a).to.be.lessThan(3);
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 2});
        test.behaviours.add('test', {a: 1});
        expect(counter).to.equal(2);

    });
    it("should fire a post initialize event on the next tick", function (done) {
        let counter = 0;
        const test = {};
        Behaviours.register("testPostInitialise", {
            postInitialize() {
                counter++;
                if (counter > 1) {
                    throw new Error("Repeated");
                }
                done();
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('testPostInitialise');
        expect(counter).to.equal(0);
    });

    it("should allow a behaviour to have default values", function () {
        const test = {};
        Behaviours.register("test", {
            defaults: {
                b: 1,
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 1});
        expect(test.behaviours.instances.test[0].b).to.equal(1);
    });
    it("should not allow a method in defaults", function () {
        expect(function () {
            Behaviours.register("test", {
                defaults: {
                    b() {
                        console.log("here");
                    },
                },
            });
        }).to.throw("Defaults must not include functions");
    });
    it("should define methods for a behaviour", function () {
        const test = {};
        Behaviours.register("test", {
            methods: {
                b() {
                    console.log("here");
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 1});
        expect(test.behaviours.instances.test[0].b).to.be.a("function");
    });
    it("should provide an object with the methods and document from initialize", ()=>{
        const test = {a: 1};
        Behaviours.register("test", {
            methods: {
                b() {
                    console.log("here");
                },
            },
        });
        let result = Behaviours.initialize(test).api;
        test.behaviours.add('test', {a: 1});
        expect(result.b).to.be.a("function");
        expect(result.document.a).to.eq(1)
        expect(result.$.a).to.eq(1)
        expect(result.setState).to.be.a("function")
    })
    it("should fire an exception if a method and value collide", function() {
        expect(function() {
            Behaviours.register("testing", {
                defaults: {
                    test: 1
                },
                methods: {
                    test() {},
                },
            });
            Behaviours.initialize({}, {
                testing: {}
            })
        }).to.throw("Member \"test\" already declared");
    });
    it("should not allow a method to be overridden", function() {
        expect(function () {
            Behaviours.register("testing", {
                methods: {
                    test() {
                    },
                },
            });
            let document = Behaviours.initialize({}, {
                testing: {}
            });
            expect(document.behaviours.instances.testing[0].test).to.be.a("function");
            document.behaviours.instances.testing[0].test = "hello";
        }).to.throw("Method \"test\" cannot be overwritten");
    });
    it("should not allow variables in methods", function () {
        expect(function () {
            Behaviours.register("test", {
                methods: {
                    b: 1,
                },
            });
        }).to.throw("Only functions may be methods");
    });
    it("should be able to send a message to a behaviour", function () {
        const test = {};
        let counter = 0;
        Behaviours.register("test", {
            methods: {
                test() {
                    counter++;
                },
            },
        });
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.add('test');
        test.behaviours.sendMessage('test');
        expect(counter).to.equal(2);
    });
    it("should have before and after functions called", function () {
        const test = {};
        let counter = 0;
        Behaviours.register("test", {
            methods: {
                before_test() {
                    counter+=2;
                },
                test() {
                    counter++;
                },
                after_test() {
                    counter += 4
                }
            },
        });
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.add('test');
        test.behaviours.sendMessage('test');
        expect(counter).to.equal(14);
    });
    it("should call behaviour messages in priority order", function () {
        let counter = 0;
        const test = {};
        Behaviours.register("test", {
            methods: {
                test() {
                    counter++;
                    expect(counter).to.equal(this.testValue);
                }
            }
        });
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test', {testValue: 2});
        test.behaviours.add('test', {_priority: 10, testValue: 1});
        test.behaviours.add('test', {_priority: 101, testValue: 3});
        test.methods.test();
        expect(counter).to.equal(3);
    });
    it("should call behaviour messages in priority order and terminate on command", function () {
        let counter = 0;
        const test = {};
        Behaviours.register("test", {
            methods: {
                test() {
                    counter++;
                    expect(counter).to.equal(this.testValue);
                    if (this.terminate) {
                        throw new Behaviours.Cancel;
                    }
                }
            }
        });
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test', {testValue: 2, terminate: true});
        test.behaviours.add('test', {_priority: 10, testValue: 1});
        test.behaviours.add('test', {_priority: 101, testValue: 3});
        test.methods.test();
        expect(counter).to.equal(2);
    });
    it("should throw non-cancel errors on to the outer program", function () {
        let counter = 0;
        const test = {};
        Behaviours.register("test", {
            methods: {
                test() {
                    counter++;
                    expect(counter).to.equal(this.testValue);
                    if (this.terminate) {
                        throw new Error("Expected");
                    }
                }
            }
        });
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test', {testValue: 2, terminate: true});
        test.behaviours.add('test', {_priority: 10, testValue: 1});
        test.behaviours.add('test', {_priority: 101, testValue: 3});
        expect(function () {
            test.methods.test();
        }).to.throw("Expected");
        expect(counter).to.equal(2);
    });
    it("should return the value from the methods", async function() {
        let counter = 0;
        const test = {};
        Behaviours.register("test", {
            methods: {
                test() {
                    return ++counter;
                }
            }
        });
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.add('test');
        expect(test.methods.test().result).to.equal(2);
        expect(test.behaviours.sendMessage("test").result).to.equal(4);
        expect((await test.behaviours.sendMessageAsync("test")).called).to.equal(2);
        expect(counter).to.equal(6);
    });
    it("should be able to call async methods", async function () {
        function sleep(time) {
            return new Promise(function (resolve) {
                setTimeout(resolve, time);
            });
        }

        const test = {};
        let counter = 0;
        Behaviours.register("test", {
            methods: {
                async test() {
                    await sleep(Math.random() * 300 + 40);
                    counter++;
                },
            },
        });
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.add('test');
        await test.behaviours.sendMessage('test');
        expect(counter).to.equal(2);
    });
    it("should call destroy when removing a behaviour", function () {
        const test = {};
        Behaviours.register("test", {
            destroy() {
                counter++;
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.add('test');
        test.behaviours.remove('test');
        expect(counter).to.equal(2);
    });
    it("should have a method on the behaviour to remove itself", function () {
        const test = {};
        Behaviours.register("test", {
            destroy() {
                counter++;
            },
            methods: {
                test() {
                    this.destroy();
                }
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.test();
        expect(test.behaviours.instances.test).to.not.exist;
        expect(counter).to.equal(1);

    });
    it("should allow behaviours to be added as an initialize parameter", function () {
        const test = {};
        Behaviours.register("test", {
            destroy() {
                counter++;
            },
            methods: {
                test() {
                    this.destroy();
                }
            }
        });
        let counter = 0;
        Behaviours.initialize(test, {test: {}});
        test.methods.test();
        expect(test.behaviours.instances.test).to.not.exist;
        expect(counter).to.equal(1);
    });
    it("should allow multiple behaviours to be added as an initialize parameter", function () {
        const test = {};
        Behaviours.register("test", {
            destroy() {
                counter++;
            },
            methods: {
                test() {
                    this.destroy();
                }
            }
        });
        let counter = 0;
        Behaviours.initialize(test, {test: [{}, {}]});
        test.methods.test();
        expect(test.behaviours.instances.test).to.not.exist;
        expect(counter).to.equal(2);
    });
    it("should call destroy when removing a specific behaviour", function () {
        const test = {};
        Behaviours.register("test", {
            destroy() {
                counter++;
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        let instance = test.behaviours.add('test');
        test.behaviours.add('test');
        test.behaviours.remove('test', instance);
        expect(counter).to.equal(1);
    });
    it("should remove all behaviours on 'destroy'", function () {
        const test = {};
        Behaviours.register("test", {
            destroy() {
                counter++;
            }
        });
        Behaviours.register("banana", {
            destroy() {
                counter++;
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.add('test');
        test.behaviours.add('banana');
        test.behaviours.destroy();
        expect(counter).to.equal(3);
    });
    it("should be able to add a custom behaviour", function () {
        const test = {};
        Behaviours.events.on('behaviour.add.testCustom', function (context, data) {
            if (data.behaviourName === 'testCustom') {
                data.availableBehaviour = {
                    methods: {
                        test() {
                            counter++;
                        }
                    }
                };
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('testCustom');
        test.methods.test();
        expect(counter).to.equal(1);
    });
    it("should have a behaviour require another behaviour", function () {
        const test = {};
        Behaviours.register("example", {
            methods: {
                example() {
                    counter++;
                }
            }
        });
        Behaviours.register("test", {
            requires: {
                example: {}
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.example();
        expect(counter).to.equal(1);
    });
    it("should require another behaviour with an array of strings", function () {
        const test = {};
        Behaviours.register("example", {
            methods: {
                example() {
                    counter++;
                }
            }
        });
        Behaviours.register("test", {
            requires: ["example"]
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.example();
        expect(counter).to.equal(1);
    })
    it("should have not add a required behaviour if there is already a behaviour with the same parameters", function () {
        const test = {};
        Behaviours.register("example", {
            methods: {
                example() {
                    counter++;
                }
            }
        });
        Behaviours.register("test", {
            requires: {
                example: {}
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('example');
        test.behaviours.add('test');
        test.methods.example();
        expect(counter).to.equal(1);
    });
    it("should be able to call a method that is declared in calls", function () {
        const test = {};
        Behaviours.register("example", {
            calls: ['banana'],
            methods: {
                example() {
                    this.api.banana()
                    counter++;
                }
            }
        });
        Behaviours.register("test", {
            requires: {
                example: {}
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('example');
        test.behaviours.add('test');
        test.methods.example();
        expect(counter).to.equal(1);
    })
    it("should call a method on another behaviour", function() {
        const test = {};
        Behaviours.register("example", {
            calls: ['banana'],
            methods: {
                example() {
                    this.api.banana()
                    counter++;
                }
            }
        });
        Behaviours.register("test", {
            requires: {
                example: {}
            },
            methods: {
                tryToCall() {
                    this.api.example();
                    this.sendMessage('example')
                }
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('example');
        test.behaviours.add('test');
        test.methods.tryToCall();
        expect(counter).to.equal(2);
    })
    it("should be able to send a message to the document", function () {
        const test = {};
        Behaviours.register("example", {
            methods: {
                example() {
                    counter++;
                }
            }
        });
        Behaviours.register("test", {
            requires: {
                example: {}
            },
            methods: {
                tryToCall() {
                    this.api.example();
                    this.sendMessage('example')
                }
            }
        });
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('example');
        test.behaviours.add('test');
        test.sendMessage('tryToCall')
        expect(counter).to.equal(2);
    })
    it("should allow us to add two implementations of a behaviour and call both", function() {
        const test = {};
        Behaviours.register("example", {
            methods: {
                example() {
                    counter++;
                }
            }
        }, true);
        Behaviours.register("example", {
            methods: {
                example() {
                    counter+=2;
                }
            }
        }, true);
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('example');
        test.methods.example();
        expect(counter).to.equal(3);
    })
    it("should be able to call multiple behaviour async methods", async function () {
        function sleep(time) {
            return new Promise(function (resolve) {
                setTimeout(resolve, time);
            });
        }

        const test = {};
        let counter = 0;
        Behaviours.register("test", {
            methods: {
                async test() {
                    await sleep(Math.random() * 300 + 40);
                    counter++;
                },
            },
        });
        Behaviours.register("test", {
            methods: {
                async test() {
                    await sleep(1);
                    counter++;
                },
            },
        }, true);
        counter = 0
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.add('test');
        await test.behaviours.sendMessage('test');
        expect(counter).to.equal(4);
    });
});
