const expect = require('chai').expect;
const Behaviours = require('../index');

describe("Persistence", function () {
    beforeEach(function () {
        Behaviours.reset();
    });
    it("should stringify a standard object", function () {
        let source = {a: 1, b: 2, c: "three"};
        let result = Behaviours.stringify(source);
        expect(result.length).to.be.greaterThan(8);
    });
    it("should parse a standard stringified object", function () {
        let source = '{"a":1,"b":2}';
        let result = Behaviours.parse(source);
        expect(result.b).to.equal(2);
        expect(result.behaviours).to.equal(undefined);
        expect(result._behaviours).to.equal(undefined);
    });
    it("should have behaviours data stored with the object", function () {
        let test = {};
        Behaviours.register("test", {
            initialize() {
                this.c = 3;
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 2, b: 1});
        let result = JSON.parse(Behaviours.stringify(test));
        expect(result._behaviours.instances.test[0].a).to.equal(2);
        expect(result._behaviours.instances.test[0].b).to.equal(1);
        expect(result._behaviours.instances.test[0].c).to.equal(3);
    });
    it("should have been restored with the behaviours available", function () {
        let test = {};
        Behaviours.register("test", {
            initialize() {
                this.c = 3;
            },
            methods: {
                test() {
                    this.d = 4;
                }
            }
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 2, b: 1});
        let result = Behaviours.parse(Behaviours.stringify(test));
        result.methods.test();
        expect(result.behaviours.instances.test[0].a).to.equal(2);
        expect(result.behaviours.instances.test[0].b).to.equal(1);
        expect(result.behaviours.instances.test[0].c).to.equal(3);
        expect(result.behaviours.instances.test[0].d).to.equal(4);
    });
    it("should be in the correct state after restore and the state enter function should not be called", function () {
        let test = {};
        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        this.e = 1;
                    },
                    methods: {
                        test() {
                            this.d = 4;
                        }
                    }
                },
                example: {
                    enter() {
                        this.e = 2;
                        this.d = 1;
                    },
                    methods: {
                        test() {
                            this.d = 5;
                        },
                        test2() {
                            this.d = 1;
                        }
                    }
                }
            }
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 2, b: 1});
        test.behaviours.state = 'example';
        test.methods.test2();
        let result = Behaviours.parse(Behaviours.stringify(test));
        expect(result.behaviours.state).to.equal('example');
        expect(result.behaviours.instances.test[0].e).to.equal(2);
        expect(result.behaviours.instances.test[0].d).to.equal(1);
        result.methods.test();
        expect(result.behaviours.instances.test[0].d).to.equal(5);

    });
    it("should not restore a temporary behaviour", function () {
        let counter = 0;
        Behaviours.register("permanent", {
            destroy() {
                counter++;
            },
            methods: {
                example() {
                    counter++;
                }
            }
        });
        Behaviours.register("temporary", {
            destroy() {
                counter++;
            },
            methods: {
                test() {
                    counter++;
                }
            }
        });
        let document = {};
        Behaviours.initialize(document);
        document.behaviours.add("permanent");
        document.behaviours.add("temporary", null, true);
        document.methods.test();
        let storage = Behaviours.stringify(document);
        document.behaviours.destroy();
        expect(counter).to.equal(3);
        let rehydrated = Behaviours.parse(storage);
        expect(function () {
            rehydrated.methods.test();
        }).to.throw("rehydrated.methods.test is not a function");
        rehydrated.methods.example();
        expect(counter).to.equal(4);

    });
    it("should allow events to modify the object during persistence", function () {
        let test = {};
        let counter = 0;
        Behaviours.register("test", {
            initialize() {
                this.c = 3;
            },
            methods: {
                test() {
                    this.d = 4;
                }
            }
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {a: 2, b: 1});
        Behaviours.events.on("behaviour.stringify", function (context, data) {
            data.source.e = 5;
            counter++;
        });
        Behaviours.events.on("behaviour.stringified", function (context, data) {
            data.result = "OUTPUT" + data.result;
            counter++;
        });
        Behaviours.events.on("behaviour.parse", function (context, data) {
            data.source = data.source.slice(6);
            counter++;
        });
        Behaviours.events.on("behaviour.parsed.pre", function (context, data) {
            expect(data.result.e).to.equal(5);
            expect(data.result.behaviours).to.not.exist;
            counter++;
        });
        Behaviours.events.on("behaviour.parsed.post", function (context, data) {
            expect(data.result.methods.test).to.be.a("function");
            counter++;
        });
        let source = Behaviours.stringify(test);
        expect(source.slice(0, 6)).to.equal("OUTPUT");
        let result = Behaviours.parse(source);
        expect(result.behaviours.instances.test[0].a).to.equal(2);
        expect(result.e).to.equal(5);
        expect(counter).to.equal(5);
    });
    it("should manage to serialize objects within that also have behaviours", function() {
        Behaviours.register("first", {
            methods: {
                testFirst(param) {
                    param.result = "first"
                }
            }
        })
        Behaviours.register("second", {
            methods: {
                testSecond(param) {
                    param.result = "second"
                }
            }
        })
        Behaviours.register("third", {
            methods: {
                testThird(param) {
                    param.result = "third"
                }
            }
        })
        let test1 = Behaviours.initialize({})
        let test2 = Behaviours.initialize({})
        let test = Behaviours.initialize({test1, test2})
        test.behaviours.add("third")
        test1.behaviours.add("first")
        test2.behaviours.add("second")
        let param = {}
        test.test1.methods.testFirst(param)
        expect(param.result).to.eq("first")
        let stored = Behaviours.stringify(test)
        let restored = Behaviours.parse(stored)
        expect(restored.test1).not.eq(test1)
        expect(restored.test2).not.eq(test2)
        restored.test1.methods.testFirst(param)
        expect(param.result).to.eq("first")
        restored.test2.methods.testSecond(param)
        expect(param.result).to.eq("second")
    })

});
