const expect = require('chai').expect;
const Behaviours = require('../index');
const {asyncMethod} = Behaviours

describe("State Machine Behaviours", function () {
    beforeEach(function () {
        Behaviours.reset();
    });
    it("should call a method based on the state", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    methods: {
                        increment() {
                            counter++;
                        },
                    },
                },
                test: {
                    methods: {
                        increment() {
                            counter2++;
                        },
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.increment();
        test.behaviours.state = 'test';
        test.methods.increment();
        test.behaviours.state = '';
        test.methods.increment();
        expect(counter).to.equal(2);
        expect(counter2).to.equal(1);
    });
    it("should call a state changed message", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        let counter3 = 0;
        Behaviours.register("test", {
            methods: {
                stateChanged() {
                    counter3++;
                }
            },
            states: {
                default: {
                    methods: {
                        increment() {
                            counter++;
                        },
                    },
                },
                test: {
                    methods: {
                        increment() {
                            counter2++;
                        },
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.increment();
        test.behaviours.state = 'test';
        test.methods.increment();
        test.behaviours.state = '';
        test.methods.increment();
        expect(counter).to.equal(2);
        expect(counter2).to.equal(1);
        expect(counter3).to.equal(2);
    });
    it("should wait for the state change complete message", async function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        let counter3 = 0;
        Behaviours.register("test", {
            methods: {
                async stateChangeComplete() {
                    await new Promise(function (resolve) {
                        setTimeout(resolve, 100);
                    });
                    counter3++;
                }
            },
            states: {
                default: {
                    methods: {
                        increment() {
                            counter++;
                        },
                    },
                },
                test: {
                    methods: {
                        increment() {
                            counter2++;
                        },
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.increment();
        await test.behaviours.setState('test');
        test.methods.increment();
        await test.behaviours.setState('default');
        test.methods.increment();
        expect(counter).to.equal(2);
        expect(counter2).to.equal(1);
        expect(counter3).to.equal(2);
    });
    it("should only allow methods in state method definitions", function () {
        expect(function () {
            Behaviours.register("test", {
                states: {
                    default: {
                        methods: {
                            a: 1,
                            increment() {
                                counter++;
                            },
                        },
                    },
                    test: {},
                },
            });
        }).to.throw("Only functions may be methods");

    });
    it("should only call a method if available", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    methods: {
                        increment() {
                            counter++;
                        },
                    },
                },
                test: {},
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.increment();
        test.behaviours.state = 'test';
        test.methods.increment();
        test.behaviours.state = '';
        test.methods.increment();
        expect(counter).to.equal(2);
        expect(counter2).to.equal(0);
    });
    it("should call a base method if a state method is not available", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            methods: {
                increment() {
                    counter2++;
                },
            },
            states: {
                default: {
                    methods: {
                        increment() {
                            counter++;
                        },
                    },
                },
                test: {},
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.increment();
        test.behaviours.state = 'test';
        test.methods.increment();
        test.behaviours.state = '';
        test.methods.increment();
        expect(counter).to.equal(2);
        expect(counter2).to.equal(1);
    });
    it("should call a state method not defined in the base", () => {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {},
                test: {
                    methods: {
                        increment() {
                            counter++;
                        },
                    }
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.methods.increment();
        test.behaviours.state = 'test';
        test.methods.increment();
        test.behaviours.state = '';
        test.methods.increment();
        expect(counter).to.equal(1);
    })
    it("should call an async method based on the state", async function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    methods: {
                        increment: async () => {
                            await new Promise(resolve => setTimeout(resolve, 100))
                            counter++;
                        },
                    },
                },
                test: {
                    methods: {
                        increment: async () => {
                            await new Promise(resolve => setTimeout(resolve, 100))
                            counter2++;
                        },
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        await test.methods.increment();
        test.setState('test');
        await test.methods.increment();
        test.setState('');
        await test.methods.increment();
        expect(counter).to.equal(2);
        expect(counter2).to.equal(1);
    });
    it("should call an enter method on entering a state", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    methods: {},
                },
                test: {
                    enter() {
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.setState('test')
        test.setState('');
        expect(counter).to.equal(2);
        expect(counter2).to.equal(1);
    });
    it("should call an exit method when leaving a state", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    exit() {
                        counter2++;
                    },
                    methods: {},
                },
                test: {
                    enter() {
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.setState('test');
        test.setState('');
        expect(counter).to.equal(2);
        expect(counter2).to.equal(2);
    });
    it("should allow a target state to refuse a switch", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    exit() {
                        counter2++;
                    },
                    methods: {},
                },
                test: {
                    canEnter(context) {
                        context.canChange = false;
                    },
                    enter() {
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.state = 'test';
        expect(test.behaviours.state).to.equal('default');
        test.behaviours.state = '';
        expect(counter).to.equal(1);
        expect(counter2).to.equal(0);
    });
    it("should have a message if the context couldn't change", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    exit() {
                        counter2++;
                    },
                    methods: {},
                },
                test: {
                    canEnter(context) {
                        context.canChange = false;
                        context.reasons.push("Test");
                    },
                    enter() {
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.state = 'test';
        expect(test.behaviours.state).to.equal('default');
        expect(test.behaviours.reasons[0]).to.equal("Test");
        test.behaviours.state = '';
        expect(counter).to.equal(1);
        expect(counter2).to.equal(0);
    });
    it("should allow a start state to refuse a switch", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    canExit(context) {
                        context.canChange = false;
                    },
                    methods: {},
                },
                test: {
                    enter() {
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.state = 'test';
        expect(test.behaviours.state).to.equal('default');
        test.behaviours.state = '';
        expect(counter).to.equal(1);
        expect(counter2).to.equal(0);
    });
    it("should be able to wait for an async state change", async function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;

        function sleep(time) {
            return new Promise(function (resolve) {
                setTimeout(resolve, time);
            });
        }

        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    methods: {},
                },
                test: {
                    async enter() {
                        await sleep(100);
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.state = 'test';
        expect(counter2).to.equal(0);
        await test.behaviours.ready;
        expect(counter2).to.equal(1);
    });
    it("should be able to wait for an async state change using a method", async function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;

        function sleep(time) {
            return new Promise(function (resolve) {
                setTimeout(resolve, time);
            });
        }

        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    methods: {},
                },
                test: {
                    async enter() {
                        await sleep(100);
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        await test.behaviours.setState('test');
        expect(counter2).to.equal(1);

    });
    it("should ensure that exit functions are finished before enter functions run", async function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;

        function sleep(time) {
            return new Promise(function (resolve) {
                setTimeout(resolve, time);
            });
        }

        Behaviours.register("test", {
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    async exit() {
                        await sleep(100);
                        counter++;
                    },
                    methods: {},
                },
                test: {
                    async enter() {
                        expect(counter).to.equal(2);
                        await sleep(100);
                        counter2++;
                    },
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test');
        test.behaviours.state = 'test';
        expect(counter2).to.equal(0);
        await test.behaviours.ready;
        expect(counter2).to.equal(1);
    });
    it("should allow a behaviour to dynamically map a state", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        Behaviours.register("test", {
            mapState(state) {
                return state === this.useState ? "activatedTest" : state;
            },
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    methods: {},
                },
                activatedTest: {
                    enter() {
                        counter2++;
                    },
                    exit() {
                        counter2++;
                    }
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {useState: "test"});
        test.behaviours.state = 'test';
        test.behaviours.state = '';
        expect(counter).to.equal(2);
        expect(counter2).to.equal(2);
    });
    it("should allow call a method on a dynamically mapped state", function () {
        const test = {};
        let counter = 0;
        let counter2 = 0;
        let counter3 = 0;
        Behaviours.register("test", {
            mapState(state) {
                return state === this.useState ? "activatedTest" : state;
            },
            states: {
                default: {
                    enter() {
                        counter++;
                    },
                    methods: {},
                },
                activatedTest: {
                    enter() {
                        counter2++;
                    },
                    exit() {
                        counter2++;
                    },
                    methods: {
                        test() {
                            counter3++;
                        }
                    }
                },
            },
        });
        Behaviours.initialize(test);
        test.behaviours.add('test', {useState: "test"});
        test.behaviours.state = 'test';
        test.methods.test();
        test.behaviours.state = '';
        expect(counter).to.equal(2);
        expect(counter2).to.equal(2);
        expect(counter3).to.equal(1);
    });
    it("should call methods in different implementation states", function () {
        const test = {};
        Behaviours.register("example", {
            states: {
                active: {
                    methods: {
                        example() {
                            counter++;
                        }
                    }
                }
            }
        }, true);
        Behaviours.register("example", {
            states: {
                active: {
                    methods: {
                        example() {
                            counter += 2;
                        }
                    }
                }
            }
        }, true);
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('example');
        test.methods.example();
        expect(counter).to.equal(0);
        test.behaviours.state = 'active'
        test.methods.example();
        expect(counter).to.equal(3);

    })
    it("should call methods in different implementation states mismatched", function () {
        const test = {};
        Behaviours.register("example", {
            states: {
                passive: {
                    methods: {
                        example() {
                            counter++;
                        }
                    }
                }
            }
        }, true);
        Behaviours.register("example", {
            states: {
                active: {
                    methods: {
                        example() {
                            counter += 2;
                        }
                    }
                }
            }
        }, true);
        let counter = 0;
        Behaviours.initialize(test);
        test.behaviours.add('example');
        test.methods.example();
        expect(counter).to.equal(0);
        test.behaviours.state = 'active'
        test.methods.example();
        expect(counter).to.equal(2);
        test.behaviours.state = 'passive'
        test.methods.example();
        expect(counter).to.equal(3);


    })

});
