"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var HookedEvents = require('alcumus-local-events/hooked-events');

var isObject = require('lodash/isObject');

var isString = require('lodash/isString');

var forEach = require('lodash/forEach');

var merge = require('lodash/merge');

var sortBy = require('lodash/sortBy');

var isArray = require('lodash/isArray');

var DEFAULT = 'default';

var Cancel =
/*#__PURE__*/
function (_Error) {
  _inherits(Cancel, _Error);

  function Cancel() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Cancel);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Cancel)).call.apply(_getPrototypeOf2, [this].concat(args)));
    Error.captureStackTrace(_assertThisInitialized(_assertThisInitialized(_this)), Cancel);
    return _this;
  }

  return Cancel;
}(_wrapNativeSuper(Error));

function ensureArray(param) {
  return Array.isArray(param) ? param : [param];
}

var availableBehaviours = {};

function doNotMapState(state) {
  return state;
}

var DUMMY = {};
var events = new HookedEvents({
  wildcard: true,
  maxListeners: 1000,
  delimiter: '.'
});

function reset() {
  events.removeAllListeners();
  availableBehaviours = {};
}

function isEqual(test, item) {
  var isSame = true;
  forEach(item, function (value, key) {
    isSame = isSame && test[key] == value;
  });
  return isSame;
}

function stringify(source, replacer, space) {
  var data = {
    source: source,
    replacer: replacer,
    space: space
  };
  events.emit('behaviour.stringify', data);
  data.result = JSON.stringify(data.source, data.replacer, data.space);
  events.emit('behaviour.stringified', data);
  return data.result;
}

function parse(source, reviver) {
  var data = {
    source: source,
    reviver: reviver
  };
  var fixUps = [];
  events.emit('behaviour.parse', data);
  data.result = JSON.parse(data.source, function (key, value) {
    if (isObject(value) && value._behaviours) {
      fixUps.push(function () {
        return initialize(value);
      });
    }

    if (data.reviver) {
      data.reviver(key, value);
    }

    return value;
  });
  events.emit('behaviour.parsed.pre', data);
  fixUps.forEach(function (fixupFunction) {
    return fixupFunction();
  });
  events.emit('behaviour.parsed.post', data);
  return data.result;
}

function resolveMethods(definition) {
  var methods = definition.methods || {};
  var allMethods = definition.allMethods = new Set();
  events.emit('default-methods', methods, definition);
  Object.keys(methods).forEach(function (key) {
    if (typeof methods[key] !== 'function') {
      throw new Error('Only functions may be methods');
    }

    allMethods.add(key);
  });
  return allMethods;
}

function register(name, definition, allowMultiple) {
  var existing = undefined;

  if (!isObject(definition)) {
    throw new Error('The definition must be an object');
  }

  if (!isString(name)) {
    throw new Error('The name must be a string');
  }

  if (availableBehaviours[name] && !allowMultiple) {
    throw new Error("Behaviour '".concat(name, "' already registered"));
  }

  var defaults = definition.defaults;

  if (defaults) {
    Object.keys(defaults).forEach(function (key) {
      if (typeof defaults[key] === 'function') {
        throw new Error('Defaults must not include functions');
      }
    });
  }

  var allMethods = resolveMethods(definition);
  var states = definition.states = definition.states || DUMMY;
  Object.keys(states).forEach(function (stateName) {
    var state = states[stateName];
    Object.keys(state.methods || DUMMY).forEach(function (key) {
      if (typeof state.methods[key] !== 'function') {
        throw new Error('Only functions may be methods');
      }

      allMethods.add(key);
    });
  });
  var list = availableBehaviours[name] = availableBehaviours[name] || [];
  list.push(definition);
}

function tryToCall(behaviourName, instance, method) {
  for (var _len2 = arguments.length, params = new Array(_len2 > 3 ? _len2 - 3 : 0), _key2 = 3; _key2 < _len2; _key2++) {
    params[_key2 - 3] = arguments[_key2];
  }

  if (events.emit.apply(events, ["".concat(behaviourName, ".").concat(method), instance].concat(params))) {
    var toCallBehaviours = ensureArray(availableBehaviours[behaviourName]);
    var called = false;
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = toCallBehaviours[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var behaviour = _step.value;

        if (behaviour) {
          var fn = behaviour[method];

          if (fn && (!fn.once || !called)) {
            called = true;
            return fn.apply(instance, params);
          }
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }
}

function initialize(target) {
  var addBehaviours = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  if (target.behaviours) return;
  var api = {};
  var temporaryInstances = [];
  var reasons = [];
  var behaviours = target._behaviours = Object.defineProperties(Object.assign({
    instances: {},
    _state: '',
    sendMessage: sendMessage,
    sendMessageAsync: sendMessageAsync,
    destroy: destroy,
    add: add,
    remove: remove,
    setState: setState
  }, target._behaviours), {
    behaviours: {
      get: function get() {
        return target._behaviours;
      }
    },
    reasons: {
      get: function get() {
        return reasons;
      }
    },
    state: {
      get: function get() {
        return behaviours._state || DEFAULT;
      },
      set: function set(endState) {
        endState = endState || DEFAULT;
        var startState = behaviours.state;
        if (startState === endState) return;
        var data = {
          canChange: true,
          startState: startState,
          endState: endState,
          reasons: []
        };
        forEachBehaviour(function (instance, behaviour) {
          var mappingFunction = (behaviour.mapState || doNotMapState).bind(instance);
          var behaviourStart = mappingFunction(startState);
          var behaviourEnd = mappingFunction(endState);
          var oldState = (behaviour.states || DUMMY)[behaviourStart];
          var newState = (behaviour.states || DUMMY)[behaviourEnd];

          if (oldState && oldState.canExit) {
            oldState.canExit.call(instance, data);
          }

          if (newState && data.canChange && newState.canEnter) {
            newState.canEnter.call(instance, data);
          }
        });
        reasons = data.reasons;
        if (!data.canChange) return;
        reasons = [];
        delete data.canChange;
        forEachBehaviour(function (instance, behaviour) {
          var mappingFunction = (behaviour.mapState || doNotMapState).bind(instance);
          var behaviourStart = mappingFunction(startState);
          var oldState = (behaviour.states || DUMMY)[behaviourStart];

          if (oldState && oldState.exit) {
            var _result = oldState.exit.call(instance, data);

            instance._ready = _result && _result.then ? _result : null;
          } else {
            instance._ready = null;
          }
        });
        behaviours._state = endState;
        behaviours.sendMessage('stateChanged', data);
        events.emit('stateChanged', target, endState, data);
        forEachBehaviour(function (instance, behaviour) {
          var mappingFunction = (behaviour.mapState || doNotMapState).bind(instance);
          var behaviourEnd = mappingFunction(endState);
          var newState = (behaviour.states || DUMMY)[behaviourEnd];

          if (newState && newState.enter) {
            if (instance._ready) {
              instance._ready = instance._ready.then(function () {
                return Promise.resolve(newState.enter.call(instance, data));
              });
            } else {
              instance._ready = Promise.resolve(newState.enter.call(instance, data));
            }
          }
        });
        behaviours.ready = Promise.all(getAllInstances().map(function (pair) {
          return pair.instance._ready;
        }).concat([behaviours.sendMessageAsync('stateChangeComplete', data)]));
      }
    }
  });
  forEach(Object.assign({}, behaviours.instances, addBehaviours), function (list, type) {
    list = isArray(list) ? list : [list];
    list.forEach(function (instance) {
      behaviours.add(type, instance);
    });
  });
  Object.defineProperties(target, {
    behaviours: {
      get: function get() {
        return behaviours;
      }
    },
    methods: {
      get: function get() {
        return api;
      }
    },
    api: {
      get: function get() {
        return api;
      }
    },
    setState: {
      get: function get() {
        return behaviours.setState;
      }
    },
    sendMessage: {
      get: function get() {
        return sendMessage;
      }
    },
    sendMessageAsync: {
      get: function get() {
        return sendMessageAsync;
      }
    }
  });
  api.setState = setState;
  api.document = api.$ = target;
  return target;

  function setState(endState) {
    behaviours.state = endState;
    return behaviours.ready;
  }

  function remove(_x) {
    return _remove.apply(this, arguments);
  }

  function _remove() {
    _remove = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(behaviourName) {
      var instance,
          current,
          list,
          instanceIndex,
          temporaryIndex,
          _args = arguments;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              instance = _args.length > 1 && _args[1] !== undefined ? _args[1] : null;

              if (!instance) {
                current = behaviours.instances[behaviourName] || [];
                current.forEach(function (instance) {
                  tryToCall(behaviourName, instance, 'destroy');
                });
                delete behaviours.instances[behaviourName];
              } else {
                list = behaviours.instances[behaviourName] = behaviours.instances[behaviourName] || [];
                instanceIndex = list.indexOf(instance);
                temporaryIndex = temporaryInstances.findIndex(function (item) {
                  return item.instance === instance;
                });

                if (instanceIndex !== -1 || temporaryIndex !== -1) {
                  tryToCall(behaviourName, instance, 'destroy');
                }

                list.splice(instanceIndex, 1);
                temporaryInstances.splice(temporaryIndex, 1);
                if (!list.length) delete behaviours.instances[behaviourName];
              }

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));
    return _remove.apply(this, arguments);
  }

  function destroy() {
    var instances = behaviours.instances;

    for (var type in instances) {
      behaviours.remove(type);
    }

    temporaryInstances.forEach(function (pair) {
      tryToCall(pair.type, pair.instance, 'destroy');
    });
    temporaryInstances.length = 0;
  }

  function getAllInstances() {
    var instances = temporaryInstances.slice(0);
    forEach(behaviours.instances, function (list, type) {
      list.forEach(function (instance) {
        instances.push({
          type: type,
          instance: instance
        });
      });
    });
    return instances;
  }

  function forEachBehaviour(cb) {
    forEach(behaviours.instances, function (list, type) {
      var toCallBehaviours = ensureArray(availableBehaviours[type]);
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        var _loop = function _loop() {
          var availableBehaviour = _step2.value;

          if (availableBehaviour) {
            list.forEach(function (instance) {
              cb(instance, availableBehaviour);
            });
          }
        };

        for (var _iterator2 = toCallBehaviours[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          _loop();
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    });
  }

  function add(behaviourName, instance) {
    var temporary = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    instance = instance || {};
    var existing = availableBehaviours[behaviourName];

    if (!existing) {
      var data = {
        availableBehaviour: null,
        behaviourName: behaviourName,
        instance: instance //Allow an event to modify the behaviour and instance

      };
      events.emit("behaviour.add.".concat(behaviourName), data);
      existing = data.availableBehaviour;
      availableBehaviours[behaviourName] = existing;
      instance = data.instance;
    }

    if (!existing && instance._mandatory !== false) {
      throw new Error("Behaviour '".concat(behaviourName, "' does not exist"));
    }

    Object.defineProperties(instance, {
      document: {
        get: function get() {
          return target;
        }
      },
      methods: {
        get: function get() {
          return api;
        }
      },
      api: {
        get: function get() {
          return api;
        }
      },
      $: {
        get: function get() {
          return target;
        }
      },
      _: {
        get: function get() {
          return api;
        }
      },
      sendMessage: {
        get: function get() {
          return sendMessage;
        }
      },
      sendMessageAsync: {
        get: function get() {
          return sendMessageAsync;
        }
      }
    });
    Object.defineProperty(instance, 'destroy', {
      get: function get() {
        return function () {
          return behaviours.remove(behaviourName, instance);
        };
      }
    });
    var addBehaviours = Array.isArray(existing) ? existing : [existing];
    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
      var _loop2 = function _loop2() {
        var availableBehaviour = _step3.value;
        merge(instance, availableBehaviour.defaults || {}, instance);

        if (availableBehaviour.calls) {
          var _iteratorNormalCompletion4 = true;
          var _didIteratorError4 = false;
          var _iteratorError4 = undefined;

          try {
            var _loop3 = function _loop3() {
              var call = _step4.value;

              api[call] = api[call] || function () {
                for (var _len3 = arguments.length, params = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
                  params[_key3] = arguments[_key3];
                }

                return sendMessage.apply(void 0, [call].concat(params));
              };
            };

            for (var _iterator4 = availableBehaviour.calls[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
              _loop3();
            }
          } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                _iterator4.return();
              }
            } finally {
              if (_didIteratorError4) {
                throw _iteratorError4;
              }
            }
          }
        }

        if (availableBehaviour.methods) {
          for (var fn in availableBehaviour.methods) {
            (function (fn) {
              var fns = instance["__".concat(fn)];

              if (instance[fn] && typeof instance[fn] !== 'function') {
                throw new Error("Member \"".concat(fn, "\" already declared"));
              }

              if (!fns) {
                var _Object$definePropert;

                var getter = function getter() {
                  var result = undefined;
                  var _iteratorNormalCompletion5 = true;
                  var _didIteratorError5 = false;
                  var _iteratorError5 = undefined;

                  try {
                    for (var _iterator5 = fns[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                      var _fn = _step5.value;

                      var newResult = _fn.apply(void 0, arguments);

                      if (result && result.then) {
                        result = result.then(Promise.resolve(newResult));
                      } else {
                        result = newResult;
                      }
                    }
                  } catch (err) {
                    _didIteratorError5 = true;
                    _iteratorError5 = err;
                  } finally {
                    try {
                      if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
                        _iterator5.return();
                      }
                    } finally {
                      if (_didIteratorError5) {
                        throw _iteratorError5;
                      }
                    }
                  }

                  return result;
                };

                fns = instance["__".concat(fn)] = [];
                getter.once = true;
                Object.defineProperties(instance, (_Object$definePropert = {}, _defineProperty(_Object$definePropert, "__".concat(fn), {
                  get: function get() {
                    return fns;
                  },
                  configurable: false,
                  enumerable: false
                }), _defineProperty(_Object$definePropert, fn, {
                  get: function get() {
                    return getter;
                  },
                  set: function set() {
                    throw new Error("Method \"".concat(fn, "\" cannot be overwritten"));
                  },
                  configurable: false,
                  enumerable: false
                }), _Object$definePropert));
              }

              fns.push(availableBehaviour.methods[fn].bind(instance));
            })(fn);
          }
        }

        if (!availableBehaviour.allMethods) resolveMethods(availableBehaviour);
        availableBehaviour.allMethods.forEach(function (method) {
          if (!api[method]) {
            api[method] = function () {
              for (var _len4 = arguments.length, params = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
                params[_key4] = arguments[_key4];
              }

              return sendMessage.apply(void 0, [method].concat(params));
            };
          }
        });

        if (availableBehaviour.mandatory === false) {
          instance._mandatory = false;
        }

        var newState = (availableBehaviour.states || DUMMY)[behaviours.state] || DUMMY;

        if (newState.enter) {
          var _data = {
            startState: null,
            endState: behaviours.state
          };
          newState.enter.call(instance, _data);
        }

        if (availableBehaviour.requires) {
          if (Array.isArray(availableBehaviour.requires)) {
            forEach(availableBehaviour.requires, function (type) {
              if (!behaviours.instances[type]) {
                behaviours.add(type, {});
              }
            });
          } else {
            forEach(availableBehaviour.requires, function (list, type) {
              list = isArray(list) ? list : [list];
              list.forEach(function (instance) {
                if (!instance.document && (!behaviours.instances[type] || -1 === behaviours.instances[type].findIndex(function (item) {
                  return isEqual(item, instance);
                }))) {
                  behaviours.add(type, instance);
                }
              });
            });
          }
        }
      };

      for (var _iterator3 = addBehaviours[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        _loop2();
      }
    } catch (err) {
      _didIteratorError3 = true;
      _iteratorError3 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
          _iterator3.return();
        }
      } finally {
        if (_didIteratorError3) {
          throw _iteratorError3;
        }
      }
    }

    function notify() {
      tryToCall(behaviourName, instance, 'initialize');
      setTimeout(function () {
        tryToCall(behaviourName, instance, 'postInitialize');
      }, 0);
    }

    if (!temporary && availableBehaviours.temporary !== true) {
      var list = behaviours.instances[behaviourName] = behaviours.instances[behaviourName] || [];

      if (list.indexOf(instance) === -1) {
        notify();
        list.push(instance);
      }
    } else {
      temporaryInstances.push({
        type: behaviourName,
        instance: instance
      });
      notify();
    }

    return instance;
  }

  function noop(v) {
    return v;
  }

  function sendMessage(message) {
    for (var _len5 = arguments.length, params = new Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
      params[_key5 - 1] = arguments[_key5];
    }

    var context = {};
    var result = undefined;
    var promises = [];
    var count = 0;
    events.emit('willSendMessage', {
      target: target,
      message: message,
      params: params,
      context: context
    });
    var toCall = sortBy(getAllInstances(), function (pair) {
      return pair.instance._priority || 100;
    });

    try {
      toCall.filter(function (t) {
        return availableBehaviours[t.type];
      }).forEach(function (_ref) {
        var type = _ref.type,
            instance = _ref.instance;
        (instance["before_".concat(message)] || noop).apply(instance, params.concat([result]));
      });
      toCall.filter(function (t) {
        return availableBehaviours[t.type];
      }).forEach(function (_ref2) {
        var type = _ref2.type,
            instance = _ref2.instance;
        var toCallBehaviours = ensureArray(availableBehaviours[type]);
        var called = false;
        var _iteratorNormalCompletion6 = true;
        var _didIteratorError6 = false;
        var _iteratorError6 = undefined;

        try {
          for (var _iterator6 = toCallBehaviours[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
            var availableBehaviour = _step6.value;
            var states = availableBehaviour.states || DUMMY;
            var mappingFunction = (availableBehaviour.mapState || doNotMapState).bind(instance);
            var behaviourState = mappingFunction(behaviours.state);
            var state = states[behaviourState || DEFAULT] || DUMMY;
            var methods = state.methods || DUMMY;
            var method = methods[message];
            var fn = instance[message];
            var before = instance["before_".concat(message)] || noop;

            var _after = instance["after_".concat(message)] || noop;

            if (events.emit.apply(events, ["".concat(type, ".").concat(message), instance].concat(params))) {
              if (method) {
                count++;
                result = method.apply(instance, params.concat([result]));

                if (result && result.then) {
                  promises.push(result);
                }
              } else if (fn && (!fn.once || !called)) {
                called = true;

                if (typeof fn === 'function') {
                  count++;
                  result = fn.apply(instance, params, params.concat([result]));

                  if (result && result.then) {
                    promises.push(result);
                  }
                }
              }
            }
          }
        } catch (err) {
          _didIteratorError6 = true;
          _iteratorError6 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion6 && _iterator6.return != null) {
              _iterator6.return();
            }
          } finally {
            if (_didIteratorError6) {
              throw _iteratorError6;
            }
          }
        }
      });
    } catch (e) {
      if (!(e instanceof Cancel)) throw e;
    }

    params.called = count;
    params.result = result;
    events.emit('hasSentMessage', {
      target: target,
      message: message,
      params: params,
      context: context,
      result: result
    });

    if (promises.length) {
      var _result2 = Promise.all(promises).then(function (r) {
        return params.length >= 1 ? Array.isArray(params[0]) ? Object.assign(params[0], {
          count: count,
          result: _result2
        }) : params : params;
      });

      after(toCall, message, params);
      return _result2;
    } else {
      after(toCall, message, params);
      return params.length >= 1 ? Array.isArray(params[0]) ? Object.assign(params[0], {
        count: count,
        result: result
      }) : params : params;
    }
  }

  function sendMessageAsync(_x2) {
    return _sendMessageAsync.apply(this, arguments);
  }

  function _sendMessageAsync() {
    _sendMessageAsync = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3(message) {
      var _len6,
          params,
          _key6,
          promises,
          count,
          context,
          toCall,
          _args3 = arguments;

      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              for (_len6 = _args3.length, params = new Array(_len6 > 1 ? _len6 - 1 : 0), _key6 = 1; _key6 < _len6; _key6++) {
                params[_key6 - 1] = _args3[_key6];
              }

              promises = [];
              count = 0;
              context = {};
              events.emit('willSendMessage', {
                target: target,
                message: message,
                params: params,
                context: context
              });
              toCall = sortBy(getAllInstances(), function (pair) {
                return pair.instance._priority || 100;
              });
              toCall.filter(function (t) {
                return availableBehaviours[t.type];
              }).forEach(function (_ref3) {
                var type = _ref3.type,
                    instance = _ref3.instance;
                (instance["before_".concat(message)] || noop).apply(instance, params);
              });
              _context3.next = 9;
              return Promise.all(toCall.filter(function (t) {
                return availableBehaviours[t.type];
              }).map(
              /*#__PURE__*/
              function () {
                var _ref5 = _asyncToGenerator(
                /*#__PURE__*/
                regeneratorRuntime.mark(function _callee2(_ref4) {
                  var type, instance, toCallBehaviours, called, _iteratorNormalCompletion7, _didIteratorError7, _iteratorError7, _iterator7, _step7, availableBehaviour, states, mappingFunction, behaviourState, state, methods, method, fn;

                  return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          type = _ref4.type, instance = _ref4.instance;
                          toCallBehaviours = ensureArray(availableBehaviours[type]);
                          called = false;
                          _iteratorNormalCompletion7 = true;
                          _didIteratorError7 = false;
                          _iteratorError7 = undefined;
                          _context2.prev = 6;
                          _iterator7 = toCallBehaviours[Symbol.iterator]();

                        case 8:
                          if (_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done) {
                            _context2.next = 24;
                            break;
                          }

                          availableBehaviour = _step7.value;
                          states = availableBehaviour.states || DUMMY;
                          mappingFunction = (availableBehaviour.mapState || doNotMapState).bind(instance);
                          behaviourState = mappingFunction(behaviours.state);
                          state = states[behaviourState || DEFAULT] || DUMMY;
                          methods = state.methods || DUMMY;
                          method = methods[message];
                          fn = instance[message];
                          _context2.next = 19;
                          return events.emitAsync.apply(events, ["".concat(type, ".").concat(message), instance].concat(params));

                        case 19:
                          if (!_context2.sent) {
                            _context2.next = 21;
                            break;
                          }

                          if (method) {
                            count++;
                            promises.push(Promise.resolve(method.apply(instance, params)));
                          } else if (fn && (!fn.once || !called)) {
                            called = true;

                            if (typeof fn === 'function') {
                              count++;
                              promises.push(Promise.resolve(fn.apply(instance, params)));
                            }
                          }

                        case 21:
                          _iteratorNormalCompletion7 = true;
                          _context2.next = 8;
                          break;

                        case 24:
                          _context2.next = 30;
                          break;

                        case 26:
                          _context2.prev = 26;
                          _context2.t0 = _context2["catch"](6);
                          _didIteratorError7 = true;
                          _iteratorError7 = _context2.t0;

                        case 30:
                          _context2.prev = 30;
                          _context2.prev = 31;

                          if (!_iteratorNormalCompletion7 && _iterator7.return != null) {
                            _iterator7.return();
                          }

                        case 33:
                          _context2.prev = 33;

                          if (!_didIteratorError7) {
                            _context2.next = 36;
                            break;
                          }

                          throw _iteratorError7;

                        case 36:
                          return _context2.finish(33);

                        case 37:
                          return _context2.finish(30);

                        case 38:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, _callee2, this, [[6, 26, 30, 38], [31,, 33, 37]]);
                }));

                return function (_x3) {
                  return _ref5.apply(this, arguments);
                };
              }()));

            case 9:
              params.called = count;
              _context3.next = 12;
              return Promise.all(promises);

            case 12:
              after(toCall, message, params);
              events.emit('hasSentMessage', {
                target: target,
                message: message,
                params: params,
                context: context,
                count: count
              });
              return _context3.abrupt("return", params.length >= 1 ? Array.isArray(params[0]) ? Object.assign(params[0], {
                count: count,
                result: result
              }) : params : params);

            case 15:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));
    return _sendMessageAsync.apply(this, arguments);
  }
}

function after(toCall, message, params) {
  var called = false;

  try {
    toCall.filter(function (t) {
      return availableBehaviours[t.type];
    }).forEach(function (_ref6) {
      var type = _ref6.type,
          instance = _ref6.instance;
      var call = instance["after_".concat(message)];

      if (call && (!call.once || !called)) {
        call.apply(instance, params);
      }
    });
  } catch (e) {
    if (!(e instanceof Cancel)) throw e;
  }
}

module.exports = {
  events: events,
  availableBehaviours: availableBehaviours,
  register: register,
  initialize: initialize,
  reset: reset,
  stringify: stringify,
  parse: parse,
  Cancel: Cancel
};